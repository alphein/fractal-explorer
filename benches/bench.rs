#[allow(unused_imports, unused_must_use)]
#[path = "../src/colors.rs"]
mod colors;
#[path = "../src/linspace.rs"]
mod linspace;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rand::Rng;

pub fn bench_colorscheme(c: &mut Criterion) {
    let max = 1000;
    let mut rng = rand::thread_rng();
    c.bench_function("get color", |b| {
        b.iter(|| {
            let value = rng.gen_range(1, max);
            let n = black_box(max);
            let cs = colors::ColorScheme::new(n as f64);
            let _ = cs.color(value);
        });
    });
}

pub fn bench_linspace(c: &mut Criterion) {
    let max = 1000;
    let mut rng = rand::thread_rng();
    c.bench_function("map linspace", |b| {
        b.iter(|| {
            let value = rng.gen_range(2, max);
            let n = black_box(max);
            let range = linspace::Linspace::new(0.0, value as f64, n).unwrap();
            let _: Vec<f64> = range.collect();
        });
    });
}

pub fn bench_splited_linspace(c: &mut Criterion) {
    let max = 1000;
    let mut rng = rand::thread_rng();
    c.bench_function("map splited linspace", |b| {
        b.iter(|| {
            let value = rng.gen_range(2, max);
            let parts: u32 = rng.gen_range(2, max);
            let n = black_box(max);
            let mut range = linspace::SplitedLinspace::new(0.0, value as f64, n, parts).unwrap();

            for i in 0..parts {
                range.set_part(i);
                for (_, _) in range {}
            }
        });
    });
}

criterion_group!(
    benches_linspace,
    bench_colorscheme,
    bench_linspace,
    bench_splited_linspace
);
criterion_main!(benches_linspace);
