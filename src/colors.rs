#[derive(Clone)]
pub struct ColorScheme {
    a: f64,
}

impl ColorScheme {
    pub fn new(x: f64) -> ColorScheme {
        let a = 255.0 / x;
        ColorScheme { a }
    }
    pub fn color(&self, x: u32) -> u8 {
        (self.a * x as f64) as u8
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn colorscheme_new_test() {
        let cs = ColorScheme::new(5.0);
        let result = 51.0;
        let error = 0.1;
        assert!(cs.a - result < error);
    }
    #[test]
    fn colorscheme_get_color_test() {
        let cs = ColorScheme::new(255.0);
        let values: Vec<u8> = (0..255).map(|x| cs.color(x)).collect();
        let correct: Vec<u8> = (0..255).collect();
        assert_eq!(values, correct);
    }
}
