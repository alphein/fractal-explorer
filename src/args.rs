use clap::{value_t, ArgMatches};
use std::fmt;

#[derive(Debug)]
pub struct Options<'a> {
    pub center_x: f64,
    pub center_y: f64,
    pub scale: (f64, f64),
    pub max_iter: u32,
    pub width: u32,
    pub height: u32,
    pub file_path: &'a str,
    pub args_to_clipboard: bool,
}

impl<'a> Options<'a> {
    pub fn from(input: &'a ArgMatches) -> Self {
        let width = value_t!(input.value_of("width"), u32).unwrap();
        let height = value_t!(input.value_of("height"), u32).unwrap();
        let _s = value_t!(input.value_of("scale"), f64).unwrap();
        let scale = check_proportion(width, height, _s);
        Self {
            center_x: value_t!(input.value_of("center-x"), f64).unwrap(),
            center_y: value_t!(input.value_of("center-y"), f64).unwrap(),
            scale,
            max_iter: value_t!(input.value_of("max-iter"), u32).unwrap(),
            width,
            height,
            file_path: input.value_of("file-path").unwrap(),
            args_to_clipboard: input.is_present("clipboard"),
        }
    }

    pub fn get_fmt_args(&self) -> String {
        format!(
            "center_x: {},
center_y: {},
scale: {},
max_iter: {},
width: {},
height: {}",
            self.center_x, self.center_y, self.scale.1, self.max_iter, self.width, self.height
        )
    }
}

impl fmt::Display for Options<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "\n|{sep}|\n| {:^10} | {:^10} | {:^10} | {:^10} | {:^10} | {:^10} |\n|{sep}|\n| {:^10} | {:^10} | {:^10} | {:^10} | {:^10} | {:^10} |\n|{sep}|\n\n output file: {}\n",
            "x", "y", "scale", "max iter", "width", "height",
            self.center_x,
            self.center_y,
            self.scale.1,
            self.max_iter,
            self.width,
            self.height,
            self.file_path,
            sep = String::from("-").repeat(77)
        )
    }
}

fn check_proportion(width: u32, height: u32, scale: f64) -> (f64, f64) {
    if width == height {
        (scale, scale)
    } else {
        let factor = width as f64 / height as f64;
        (scale * factor, scale)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_proportion_equal_test() {
        let w = 600u32;
        let h = 600u32;
        let s = 0.5;

        let (sw, sh) = check_proportion(w, h, s);
        assert_eq!(sw, s);
        assert_eq!(sh, s);
    }

    #[test]
    fn check_proportion_diff_test() {
        let w = 600u32;
        let h = 300u32;
        let s = 0.5;

        let (sw, sh) = check_proportion(w, h, s);
        assert_eq!(sw, 1.0);
        assert_eq!(sh, s);
    }
}
