use std::io::ErrorKind;

#[derive(Clone, Copy)]
pub struct Linspace {
    step: f64,
    start: f64,
    dim: u32,
    counter: u32,
}

impl Linspace {
    pub fn new(center: f64, scale: f64, dim: u32) -> Result<Self, ErrorKind> {
        match scale <= 0.0 && dim < 1 {
            false => Ok(Self {
                step: (2.0 * scale) / ((dim - 1) as f64),
                start: center - scale,
                dim,
                counter: 0,
            }),
            true => Err(ErrorKind::InvalidData),
        }
    }
}

impl Iterator for Linspace {
    type Item = f64;

    fn next(&mut self) -> Option<Self::Item> {
        if self.counter < self.dim {
            self.counter += 1;
            Some(self.start + (self.step * ((self.counter - 1) as f64)))
        } else {
            None
        }
    }
}

#[cfg(feature = "multithread")]
#[derive(Clone, Copy)]
pub struct SplitedLinspace {
    step: f64,
    start: f64,
    dim: u32,
    counter: u32,
    part_size: u32,
    part_block: u32,
}

#[cfg(feature = "multithread")]
impl SplitedLinspace {
    pub fn new(center: f64, scale: f64, dim: u32, parts: u32) -> Result<Self, ErrorKind> {
        match scale <= 0.0 && dim < 1 && parts < 1 {
            false => Ok(Self {
                step: (2.0 * scale) / ((dim - 1) as f64),
                start: center - scale,
                dim,
                counter: 0,
                part_size: 0,
                part_block: (dim / parts),
            }),
            true => Err(ErrorKind::InvalidData),
        }
    }

    pub fn set_part(&mut self, part: u32) {
        self.counter = self.part_block * (part);
        let total = self.counter + self.part_block;
        self.part_size = if total + self.part_block <= self.dim {
            total
        } else {
            self.dim
        };
    }

    pub fn get_size_part(&self) -> u32 {
        self.part_size - self.counter
    }
}

#[cfg(feature = "multithread")]
impl Iterator for SplitedLinspace {
    type Item = (u32, f64);

    fn next(&mut self) -> Option<Self::Item> {
        if self.counter < self.part_size {
            self.counter += 1;
            Some((
                self.counter - 1,
                self.start + (self.step * ((self.counter - 1) as f64)),
            ))
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn linspace_test() {
        let lspace = Linspace::new(0., 2., 5).unwrap();
        let arr: Vec<f64> = lspace.collect();
        let correct: Vec<f64> = vec![-2.0, -1.0, 0.0, 1.0, 2.0];
        assert_eq!(arr, correct)
    }

    #[test]
    fn splitted_linspace_test() {
        let parts = 3u32;
        let error = 0.1f64;
        let mut spl = SplitedLinspace::new(0., 2., 6, parts).unwrap();

        let correct: Vec<Vec<(u32, f64)>> = vec![
            vec![(0, -2.0), (1, -1.2)],
            vec![(2, -0.4), (3, 0.4)],
            vec![(4, 1.2), (5, 2.0)],
        ];

        for i in 0..3 {
            spl.set_part(i);
            let part_vec = &correct[i as usize];
            for ((indx, value), (c_indx, c_value)) in spl.zip(part_vec) {
                assert_eq!(indx, *c_indx);
                assert!(value - c_value < error);
            }
        }
    }
}
