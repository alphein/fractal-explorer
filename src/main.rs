mod args;
mod colors;
mod compute;
mod linspace;

use clap::{load_yaml, App};
use image::{ImageBuffer, Rgb, RgbImage};
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use std::io::Write;
use std::process::{Command, Stdio};
use std::sync::mpsc;
use std::thread;

fn main() {
    let yml = load_yaml!("args.yml");
    let app = App::from_yaml(yml).get_matches();
    let opt = args::Options::from(&app);
    println!("{}", opt);

    let (scale_x, scale_y) = opt.scale;
    let y_range = linspace::Linspace::new(opt.center_y, scale_y, opt.height)
        .expect("erro ao inicializar os valores do eixo dos imaginários");

    let cs = colors::ColorScheme::new(opt.max_iter as f64);
    let (tx, rx) = mpsc::channel::<compute::Item>();

    let multipb = MultiProgress::new();
    let sty = ProgressStyle::default_bar()
        .template("{msg:<9} => [{elapsed:^6} {bar:40.cyan/magenta} {percent:>5}%  ]")
        .progress_chars("-}=");

    #[cfg(feature = "multithread")]
    {
        let nthreads = num_cpus::get() as u32;
        let mut x_range =
            linspace::SplitedLinspace::new(opt.center_x, scale_x, opt.width, nthreads)
                .expect("erro ao inicializar os valores do eixo dos reais");
        compute::compute_fractal(
            &multipb,
            &sty,
            &mut x_range,
            y_range,
            cs,
            opt.max_iter,
            nthreads,
            tx,
        );
    }
    #[cfg(feature = "singlethread")]
    let hand = {
        let x_range = linspace::Linspace::new(opt.center_x, scale_x, opt.width)
            .expect("erro ao inicializar os valores do eixo dos imaginários");
        let colorschm = cs.clone();
        let snd = tx.clone();
        let handler = compute::fractal_single(x_range, y_range, colorschm, opt.max_iter, snd);
        handler
    };

    let w = opt.width;
    let h = opt.height;
    let file_path = String::from(opt.file_path);
    let total_loops = w * h;
    let pb = multipb.add(ProgressBar::new(total_loops as u64));

    pb.set_style(sty);
    pb.set_message("comp img");
    pb.set_draw_delta(total_loops as u64 / 100);
    thread::spawn(move || {
        let mut img: RgbImage = ImageBuffer::new(w, h);
        for _ in 0..total_loops {
            let value = rx.recv().expect("erro ao receber dados de alguma thread");
            let px = img.get_pixel_mut(value.x, value.y);
            *px = Rgb([value.z, value.z, value.z]);

            pb.inc(1)
        }
        img.save(file_path).expect("erro ao salvar a imagem");
        pb.inc(1);
        pb.finish_with_message("img saved");
    });

    multipb.join().expect("erro nas threads");

    #[cfg(feature = "singlethread")]
    hand.join().unwrap();

    if opt.args_to_clipboard {
        let args_str = opt.get_fmt_args();

        let mut inpt = Command::new("xclip")
            .args(&["-sel", "clip"])
            .arg("-i")
            .stdin(Stdio::piped())
            .spawn()
            .ok()
            .expect("erro ao preparar o comando `xclip`");

        let inpt_child = inpt.stdin.as_mut().expect("erro ao acessar o stdin");

        inpt_child
            .write_all(args_str.as_bytes())
            .ok()
            .expect("erro ao executar o comando `xclip`");
    }
}
