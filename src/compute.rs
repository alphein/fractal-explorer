use super::colors;
use super::linspace;
#[cfg(feature = "multithread")]
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use num_complex::Complex;
use num_traits::pow;
use std::sync::mpsc;
#[cfg(feature = "multithread")]
use std::sync::Arc;
use std::thread;

fn mandel(re: f64, imag: f64, max_iter: u32) -> u32 {
    let mut z = Complex::new(re, imag);
    let c = z;
    let mut counter = 0u32;
    while counter < max_iter && z.norm() <= 2.0 {
        z = pow(z, 2) + c;
        counter += 1;
    }
    counter
}

pub struct Item {
    pub x: u32,
    pub y: u32,
    pub z: u8,
}

#[cfg(feature = "multithread")]
pub fn compute_fractal(
    multipb: &MultiProgress,
    sty: &ProgressStyle,
    x_range: &mut linspace::SplitedLinspace,
    y_range: linspace::Linspace,
    cs: colors::ColorScheme,
    maxiter: u32,
    nthreads: u32,
    tx: mpsc::Sender<Item>,
) {
    let max_iter = Arc::new(maxiter);

    let mut xrange = x_range.clone();

    for t in 0..nthreads {
        xrange.set_part(t);

        let transm = tx.clone();
        let maxiter = max_iter.clone();
        let cs_clone = cs.clone();

        let size_part = xrange.get_size_part() as u64;
        let pb = multipb.add(ProgressBar::new(size_part));
        pb.set_style(sty.clone());
        pb.set_message(&format!("thread {}", t + 1));
        pb.set_draw_delta(size_part / 100);

        thread::spawn(move || {
            for (x, re) in xrange {
                for (y, imag) in y_range.enumerate() {
                    let value = mandel(re, imag, *maxiter);
                    let color = cs_clone.color(value);
                    transm
                        .send(Item {
                            x: x as u32,
                            y: y as u32,
                            z: color,
                        })
                        .unwrap();
                }
                pb.inc(1);
            }
            pb.finish_with_message("done");
        });
    }
}

#[cfg(feature = "singlethread")]
pub fn fractal_single(
    x_range: linspace::Linspace,
    y_range: linspace::Linspace,
    cs: colors::ColorScheme,
    maxiter: u32,
    tx: mpsc::Sender<Item>,
) -> thread::JoinHandle<()> {
    let handler = thread::spawn(move || {
        for (x, real) in x_range.enumerate() {
            for (y, imag) in y_range.enumerate() {
                let value = mandel(real, imag, maxiter);
                let color = cs.color(value);
                tx.send(Item {
                    x: x as u32,
                    y: y as u32,
                    z: color,
                })
                .unwrap();
            }
        }
    });
    handler
}
